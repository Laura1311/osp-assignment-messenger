package ro.cs.upb.lab3.messenger;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import java.io.*;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommunicationService extends Service {
    private final MyBinder myBinder = new MyBinder();
    private Handler callback;
    private Socket socket = null;
    private BufferedOutputStream bufferedOutputStream;
    private InputStreamReader inputStreamReader;
    private LocalBroadcastManager broadcaster;
    private RegisterObject registerObject;

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        try {
            if (socket != null) {
                socket.close();
            }

        } catch (IOException e) {
            Log.d("CommunicationService", "SOCKET CLOSE " + e.getMessage());
        }
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void initCommunicationChannel(Handler receiveMessageHandler) {
        this.callback = receiveMessageHandler;

        broadcaster = LocalBroadcastManager.getInstance(this);

        ReceiveMessageThread r = new ReceiveMessageThread(receiveMessageHandler);
        new Thread(r).start();
    }

    public void sendMessage(MessageObject messageObject, Handler mHandler) {
        try {
            PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
            printWriter.println(messageObject.getOutgoingMessageObject().toString() + '\0');
        } catch (JSONException e) {
            Log.d("CommunicationService: ", e.getMessage());
        }
    }

    public void registerUser(RegisterObject registerObject, Handler mHandler) {
        this.callback = mHandler;
        new ConnectionThread().execute(registerObject);
    }

    public void loginUser(RegisterObject registerObject, Handler mHandler) {
        this.callback = mHandler;

        new ConnectionThread().execute(registerObject);
    }

    public void reconnectToServer() {
        new ReconnectThread().execute(this.registerObject);
    }

    private class ReconnectThread extends AsyncTask<RegisterObject, Void, String> {

        @Override
        protected String doInBackground(RegisterObject... params) {
            try {
                // Wait 5 seconds for the wifi connection to finalize
                SystemClock.sleep(5000);

                //Android emulator runs in a Virtual Machine so the emulator will have its own loopback address
                if (socket != null)
                    socket.close();
                socket = new Socket(Constants.SERVER_IP, Constants.SERVER_PORT);
                bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
                inputStreamReader = new InputStreamReader(socket.getInputStream());

                PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
                printWriter.println(params[0].getRegisterObject().toString() + '\0');

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                int length = 256;
                char[] output = new char[length];
                bufferedReader.read(output, 0, length);

                String stringOutput = String.valueOf(output);

                Matcher matcher = Pattern.compile("\\{.*?\\}").matcher(stringOutput);

                if (matcher.find()) {
                    return matcher.group();
                }
            } catch (IOException e) {
                Log.d("ReconnectThread", "SOCKET CREATION ERROR " + e.getMessage());
            } catch (JSONException e) {
                Log.d("ReconnectThread", e.getMessage());
            }

            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            Log.d("ReconnectThread", "onPostExecute");

            if (response != null) {
                Log.d("ReconnectThread", response);

                try {
                    ResponseObject responseObject = new ResponseObject(response);
                    if (responseObject.getResponseCode() == 0) {
                        Toast.makeText(UserListActivity.context, "Reconnect successful", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Log.d("ReconnectThread", e.getMessage());
                }
            }

            Toast.makeText(UserListActivity.context, "Reconnect failed", Toast.LENGTH_SHORT).show();
        }

    }

    private class ConnectionThread extends AsyncTask<RegisterObject, Void, String> {

        @Override
        protected String doInBackground(RegisterObject... params) {
            try {
                if (socket == null) {
                    try {
                        //Android emulator runs in a Virtual Machine so the emulator will have its own loopback address
                        socket = new Socket(Constants.SERVER_IP, Constants.SERVER_PORT);
                        bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
                        inputStreamReader = new InputStreamReader(socket.getInputStream());

                    } catch (IOException e) {
                        Log.d("CommunicationService", "SOCKET CREATION ERROR " + e.getMessage());
                    }
                }

                PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
                printWriter.println(params[0].getRegisterObject().toString() + '\0');

                // Save the register object for later use. E.g. in case of disconnects
                registerObject = new RegisterObject(params[0], false);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                int length = 256;
                char[] output = new char[length];
                bufferedReader.read(output, 0, length);

                String stringOutput = String.valueOf(output);

                Matcher matcher = Pattern.compile("\\{.*?\\}").matcher(stringOutput);

                if (matcher.find()) {
                    return matcher.group();
                }

            } catch (IOException e) {
                Log.d("CommunicationService: ", "SOCKET CREATION ERROR" + e.getMessage());
            } catch (JSONException e) {
                Log.d("CommunicationService: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            Log.d("CommunicationService: ", "onPostExecute");

            if (response != null) {
                Log.d("CommunicationService: ", response);

                Message msg = callback.obtainMessage();
                Bundle args = new Bundle();
                try {
                    args.putParcelable(Constants.SERVER_RESPONSE, new ResponseObject(response));
                } catch (JSONException e) {
                    Log.d("CommunicationService: ", e.getMessage());
                }
                msg.setData(args);
                msg.sendToTarget();
            }
        }
    }

    private class ReceiveMessageThread implements Runnable {
        Handler receiveMessageHandler;

        public ReceiveMessageThread(Handler receiveMessageHandler) {
            this.receiveMessageHandler = receiveMessageHandler;
        }

        @Override
        public void run() {
            try {

                while (true) {
                    if (inputStreamReader.ready()) {
                        int length = 1024;
                        char[] output = new char[length];
                        inputStreamReader.read(output, 0, length);
                        String stringOutput = String.valueOf(output);
                        Log.d("ReceiveMessageThread", "stringOutput: " + stringOutput);

                        Matcher matcher = Pattern.compile("\\{.*?\\}").matcher(stringOutput);

                        while (matcher.find()) {
                            String result = matcher.group();
                            Log.d("ReceiveMessageThread", "matched string: " + result);
                            updateUi(result);
                        }
                    }
                }

            } catch (IOException e) {
                Log.d("CommunicationService: ", "SOCKET CREATION ERROR" + e.getMessage());
            }
        }


        private void updateUi(String response) {

            Log.d("CommunicationService", "updateUi");
            if (response != null) {
                Log.d("CommunicationService: ", response);

                Message msg = callback.obtainMessage();
                Bundle args = new Bundle();
                try {
                    args.putParcelable(Constants.SERVER_RESPONSE, new ResponseObject(response));
                } catch (JSONException e) {
                    Log.d("CommunicationService: ", e.getMessage());
                }
                msg.setData(args);
                msg.sendToTarget();
            }

        }
    }

    public class MyBinder extends Binder {
        CommunicationService getService() {
            return CommunicationService.this;
        }
    }

    public LocalBroadcastManager getBroadcaster() {
        return broadcaster;
    }
}
