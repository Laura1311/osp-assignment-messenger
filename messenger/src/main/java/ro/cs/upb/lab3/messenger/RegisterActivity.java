package ro.cs.upb.lab3.messenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;

import java.util.ArrayList;


public class RegisterActivity extends Activity {

    private EditText username;

    final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            ResponseObject response = bundle.getParcelable(Constants.SERVER_RESPONSE);

            //check for errors
            if (response.getResponseCode() != 0) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(RegisterActivity.this);
                dialog.setTitle("Cannot create user");
                dialog.setMessage(response.getResponseMessage());

                AlertDialog alertDialog = dialog.create();
                alertDialog.show();
            } else {
                //no need to redirect to login because server logs in every user that is registered with success
//                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                Intent intent = new Intent(RegisterActivity.this, UserListActivity.class);

                intent.putExtra(Constants.USER_LIST, (ArrayList<String>) response.getUserList());
                intent.putExtra(Constants.LOGGED_USER, username.getText().toString());
                startActivity(intent);
            }
        }
    };

    CommunicationService communicationService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = (EditText) findViewById(R.id.register_username);
        final EditText password = (EditText) findViewById(R.id.register_password);
        final EditText retypedPassword = (EditText) findViewById(R.id.register_retype_password);
        Button createUserButton = (Button) findViewById(R.id.create_user);

        communicationService = LoginActivity.getCommunicationService();

        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateInput(password, retypedPassword)) return;

                //create user on the server side
                try {
                    RegisterObject registerObject = new RegisterObject(username.getText().toString(),
                            password.getText().toString(), true);

                    if (communicationService != null) {
                        communicationService.registerUser(registerObject, mHandler);
                    }
                } catch (JSONException e) {
                    Log.d("RegisterActivity: ", "COULD NOT CREATE REGISTER OBJECT");
                }


            }
        });
    }

    private boolean validateInput(EditText password, EditText retypedPassword) {
        if (password.getText() == null || retypedPassword.getText() == null || password.getText().toString().equals("") || retypedPassword.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
            return true;
        }

        if (!password.getText().toString().equals(retypedPassword.getText().toString())) {
            Toast.makeText(RegisterActivity.this, "PASSWORD MUST BE THE SAME", Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }
}
