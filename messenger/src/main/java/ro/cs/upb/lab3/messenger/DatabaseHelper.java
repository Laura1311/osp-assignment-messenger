package ro.cs.upb.lab3.messenger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @version $Id$
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "favusers.db";
    private static final int DB_VERSION = 2;

    protected final static String NAME = "name";
    protected final static String SENDER = "sender";
    protected final static String RECEIVER = "receiver";
    protected final static String MESSAGE = "message";

    public DatabaseHelper(Context context) {
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS favusers (" +
                "id INTEGER PRIMARY KEY," +
                NAME + " TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS messages (" +
                "id INTEGER PRIMARY KEY," +
                SENDER + " TEXT, " +
                RECEIVER + " TEXT, " +
                MESSAGE + " TEXT " +
                ");");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS favusers");
        db.execSQL("DROP TABLE IF EXISTS messages");
        onCreate(db);
    }
}
