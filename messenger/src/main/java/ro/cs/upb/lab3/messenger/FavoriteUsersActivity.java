package ro.cs.upb.lab3.messenger;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;


public class FavoriteUsersActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_users);


        TextView textView = (TextView) findViewById(R.id.favusers_info);

        ListView listView = (ListView) findViewById(R.id.favorite_listview);

        List<String> userList = getFavoriteUsers();
        if (userList.isEmpty()) {
            textView.setVisibility(View.VISIBLE);
        }

        Intent intent = getIntent();

        UserAdapter arrayAdapter = new UserAdapter(this, userList, true, intent.getStringExtra(Constants.LOGGED_USER));
        listView.setAdapter(arrayAdapter);

    }

    public List<String> getFavoriteUsers() {

        List<String> favoriteUsers = new LinkedList<String>();
        Cursor cursor = getContentResolver().query(Uri.parse(MyContentProvider.favusersTableUri), new String[]{}, "", new String[]{}, "");

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.NAME));
            favoriteUsers.add(name);
        }
        cursor.close();

        return favoriteUsers;
    }
}
