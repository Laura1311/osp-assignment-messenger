package ro.cs.upb.lab3.messenger;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


public class RegisterObject extends JSONObject implements Parcelable {

    private String username;
    private String password;
    private boolean create;

    public RegisterObject(String username, String password, boolean create) throws JSONException {
        this.username = username;
        this.password = password;
        this.create = create;
    }

    public RegisterObject(RegisterObject registerObject, boolean create) {
        this.username = registerObject.username;
        this.password = registerObject.password;
        this.create = create;
    }

    public RegisterObject(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
        this.create = in.readByte() != 0;


    }

    public JSONObject getRegisterObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(Constants.USERNAME, this.username);
        jsonObject.put(Constants.PASSWORD, this.password);
        if (this.create) {
            jsonObject.put(Constants.CREATE, true);
        }

        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeByte((byte) (this.create ? 1 : 0));
    }
    public static final Creator CREATOR = new Creator() {
        public RegisterObject createFromParcel(Parcel in) {
            return new RegisterObject(in);
        }

        public RegisterObject[] newArray(int size) {
            return new RegisterObject[size];
        }
    };

}
