package ro.cs.upb.lab3.messenger;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResponseObject extends JSONObject implements Parcelable {

    private String responseString;

    private JSONObject responseJson;

    public ResponseObject(String responseString) throws JSONException {
        this.responseString = responseString;
        this.responseJson = new JSONObject(responseString);
    }

    public ResponseObject(Parcel in) {
        this.responseString = in.readString();
    }

    public int getResponseCode() {
        try {
            return this.responseJson.getInt("response");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Constants.INVALID_SERVER_RESPONSE;
    }

    public String getResponseMessage() {
        try {
            return this.responseJson.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getIncomingMessage() {
        try {
            if (responseJson.getInt("type") == 1) {
                return this.responseJson.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getUserMessage() {
        try {
            if (responseJson.getInt("type") == 1) {
                return this.responseJson.getString("user");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getUserList() {
        JSONArray users = null;
        try {
            users = this.responseJson.getJSONArray("users");
            List<String> userList = new ArrayList<String>();
            for (int i = 0; i < users.length(); i++) {
                userList.add(users.get(i).toString());
            }

            return userList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.responseString);
    }

    public static final Creator CREATOR = new Creator() {
        public ResponseObject createFromParcel(Parcel in) {
            return new ResponseObject(in);
        }

        public ResponseObject[] newArray(int size) {
            return new ResponseObject[size];
        }
    };

}
