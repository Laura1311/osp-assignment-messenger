package ro.cs.upb.lab3.messenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

public class NetworkConnectionReceiver extends BroadcastReceiver {
    public NetworkConnectionReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.wifi.STATE_CHANGE") && LoginActivity.logged) {
            Log.d("NetConReceiver", intent.getAction());
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifi.isWifiEnabled()) {
                String ssid = wifi.getConnectionInfo().getSSID();
                if (ssid != null && ssid.length() > 5 && !ssid.equals("<unknown ssid>")) {
                    if (!UserListActivity.isConnected()) {
                        Log.d("NetConReceiver", "You are connected to WIFI " + wifi.getConnectionInfo().getSSID());

                        UserListActivity.reconnect();
                    }
                }
            }
            else
            {
                Log.e("NetConReceiver", "You are NOT connected to WIFI");
                UserListActivity.disconnect();
            }
        }
    }
}
