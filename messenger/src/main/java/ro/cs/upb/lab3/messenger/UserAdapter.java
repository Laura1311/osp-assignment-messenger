package ro.cs.upb.lab3.messenger;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends BaseAdapter {
    private List<String> items;
    private Context context;
    private boolean isFavorite;
    private String loggedUser;

    public UserAdapter(Context context, List<String> items, boolean isFavorite, String loggedUser) {
        this.context = context;
        this.items = items;
        this.isFavorite = isFavorite;
        this.loggedUser = loggedUser;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < this.items.size())
            return this.items.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item, null);
        }

        final String currentItem = items.get(position);

        Button userName = (Button) convertView.findViewById(R.id.userlist_item);
        final Button markFavUser = (Button) convertView.findViewById(R.id.mark_favorite);
        final Button unmarkFavUser = (Button) convertView.findViewById(R.id.unmark_favorite);
        userName.setText(currentItem);

        if (isFavorite) {
            markFavUser.setVisibility(View.INVISIBLE);
            unmarkFavUser.setVisibility(View.INVISIBLE);
        }

        if (isUserFavorite(currentItem)) {
            markFavUser.setTextColor(Color.GRAY);
            unmarkFavUser.setTextColor(Color.BLACK);
        } else {
            markFavUser.setTextColor(Color.BLACK);
            unmarkFavUser.setTextColor(Color.GRAY);
        }

        markFavUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (markFavUser.getCurrentTextColor() == Color.BLACK) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelper.NAME, currentItem);
                    Uri uri = context.getContentResolver().insert(Uri.parse(MyContentProvider.favusersTableUri), contentValues);

                    Log.d("Add favorite user: ", uri.toString());
                    markFavUser.setTextColor(Color.GRAY);
                    unmarkFavUser.setTextColor(Color.BLACK);
                }
            }
        });

        unmarkFavUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unmarkFavUser.getCurrentTextColor() == Color.BLACK) {
                    String where = DatabaseHelper.NAME + "=?";
                    String[] selection = new String[]{currentItem};
                    int res = context.getContentResolver().delete(Uri.parse(MyContentProvider.favusersTableUri), where, selection);

                    Log.d("Delete favorite user: ", String.valueOf(res));
                    unmarkFavUser.setTextColor(Color.GRAY);
                    markFavUser.setTextColor(Color.BLACK);
                }
            }
        });


        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ChatActivity.class);
                ArrayList<String> targetUsers = new ArrayList<String>();
                targetUsers.add(currentItem);
                intent.putExtra(Constants.TARGET_USERS, targetUsers);
                intent.putExtra(Constants.LOGGED_USER, loggedUser);

                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private boolean isUserFavorite(String currentItem) {
        String selection = DatabaseHelper.NAME + "=?";
        String[] selectionArgs = new String[]{currentItem};
        Cursor cursor = context.getContentResolver().query(Uri.parse(MyContentProvider.favusersTableUri), new String[] {}, selection, selectionArgs, "");
        return cursor.moveToNext();
    }
}
