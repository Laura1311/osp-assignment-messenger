package ro.cs.upb.lab3.messenger;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;


public class UserListActivity extends AppCompatActivity {
    private static String loggedUser;
    private static Boolean logged;

    private CommunicationService communicationService;
    public static Context context;

    private static String selectedUser = "";

    public static String getSelectedUser() {
        return selectedUser;
    }

    public static void selectUser(String user) {
        selectedUser = user;
    }

    public static void unselectUser() {
        selectedUser = "";
    }


    public static Boolean isConnected() {
        return UserListActivity.logged;
    }

    public static void disconnect() {
        UserListActivity.logged = false;
    }

    public static void reconnect() {
        LoginActivity.getCommunicationService().reconnectToServer();
        UserListActivity.logged = true;
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            ResponseObject response = bundle.getParcelable(Constants.SERVER_RESPONSE);

            if (response != null) {
                response.getUserMessage();

                // Insert the message into the database
                String sender = response.getUserMessage();
                String message = response.getIncomingMessage();
                Uri uri = insertMessage(sender, loggedUser, message);
                Log.d("Added a message", uri.toString());

                // Send a broadcast to update the ui if the current activity is ChatActivity
                if (!sender.equals("")) {
                    LocalBroadcastManager broadcastManager = communicationService.getBroadcaster();
                    Intent broadcast = new Intent(Constants.UPDATE_UI);
                    broadcastManager.sendBroadcast(broadcast);
                }

                // Show a notification if the current conversation
                // is not with the sender of the message
                if (!sender.equals(selectedUser)) {

                    //Show a notification
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle("New message from " + sender.toString())
                                    .setContentText("Message: " + message.toString());
                    // Creates an explicit intent for an Activity in your app
                    Intent resultIntent = new Intent(context, ChatActivity.class);
                    ArrayList<String> targetUsers = new ArrayList<String>();
                    targetUsers.add(sender);
                    resultIntent.putExtra(Constants.TARGET_USERS, targetUsers);
                    resultIntent.putExtra(Constants.LOGGED_USER, loggedUser);

                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    context,
                                    0,
                                    resultIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotifyMgr =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    mNotifyMgr.notify(0, mBuilder.build());
                }
            }
        }
    };

    private Uri insertMessage(String sender, String receiver, String message) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.SENDER, sender);
        contentValues.put(DatabaseHelper.RECEIVER, receiver);
        contentValues.put(DatabaseHelper.MESSAGE, message);
        return this.getContentResolver().insert(Uri.parse(MyContentProvider.messagesTableUri), contentValues);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlist);

        UserListActivity.context = this;
        UserListActivity.logged = true;

        LoginActivity.logged = true;

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        actionBar.show();

        ListView listView = (ListView) findViewById(R.id.users_listview);

        Intent intent = getIntent();

        if (intent != null) {
            ArrayList<String> userList = intent.getStringArrayListExtra(Constants.USER_LIST);
            loggedUser = intent.getStringExtra(Constants.LOGGED_USER);
            userList.remove(loggedUser.toString());
            UserAdapter arrayAdapter = new UserAdapter(this, userList, false, loggedUser);
            listView.setAdapter(arrayAdapter);
        }

        // Start listening for messages
        communicationService = LoginActivity.getCommunicationService();
        communicationService.initCommunicationChannel(mHandler);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginActivity.logged = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite_users) {
            Intent intent = new Intent(UserListActivity.this, FavoriteUsersActivity.class);
            intent.putExtra(Constants.LOGGED_USER, loggedUser);
            startActivity(intent);
        } else if (id == R.id.action_logout) {
            // Kill the current activity
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
