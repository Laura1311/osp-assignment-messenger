package ro.cs.upb.lab3.messenger;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends Activity {

    CommunicationService communicationService;
    private static ListView conversation;
    public static Context context;
    private String loggedUser;
    private String targetUser;
    private ArrayAdapter<String> adapter;
    private BroadcastReceiver broadcastReceiver;

    public static void addToConversation(ResponseObject response) {
        if (response.getIncomingMessage() != null) {
            response.getUserMessage();

            TextView textView = new TextView(context);
            textView.setHeight(R.id.wrap_content);
            textView.setWidth(R.id.wrap_content);
            textView.setText(response.getIncomingMessage());

            conversation.addView(textView);
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            ResponseObject response = bundle.getParcelable(Constants.SERVER_RESPONSE);

            if (response != null) {
                response.getUserMessage();

                String sender = response.getUserMessage();
                String message = response.getIncomingMessage();
                Uri uri = insertMessage(sender, loggedUser, message);
                updateAdapter();

                Log.d("Add message: ", uri.toString());
            }
        }
    };

    private Uri insertMessage(String sender, String receiver, String message) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.SENDER, sender);
        contentValues.put(DatabaseHelper.RECEIVER, receiver);
        contentValues.put(DatabaseHelper.MESSAGE, message);
        return context.getContentResolver().insert(Uri.parse(MyContentProvider.messagesTableUri), contentValues);
    }

    private void updateAdapter() {
        adapter.clear();
        adapter.addAll(getAllMessages());
        adapter.notifyDataSetChanged();
        conversation.setSelection(adapter.getCount() - 1);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        context = this;


        final EditText messageText = (EditText) findViewById(R.id.outgoing_message);
        Button sendMessageButton = (Button) findViewById(R.id.send_message);

        communicationService = LoginActivity.getCommunicationService();

        MessageObject messageObject = null;

        Intent intent = getIntent();
        if (intent != null) {
            ArrayList<String> targetUsers = intent.getStringArrayListExtra(Constants.TARGET_USERS);
            loggedUser = intent.getStringExtra(Constants.LOGGED_USER);
            targetUser = targetUsers.get(0);
            messageObject = new MessageObject(targetUsers, messageText.getText().toString());
        }

        conversation = (ListView) findViewById(R.id.conversation);
        adapter = new ArrayAdapter<String>(context, R.layout.message_list_item, getAllMessages());
        conversation.setAdapter(adapter);

        final MessageObject finalMessageObject = messageObject;
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalMessageObject != null) {
                    String message = messageText.getText().toString();
                    finalMessageObject.setMessage(message);
                    communicationService.sendMessage(finalMessageObject, mHandler);
                    insertMessage(loggedUser, targetUser, message);
                    updateAdapter();
                    messageText.setText("");
                }
            }
        });

        UserListActivity.selectUser(targetUser);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constants.UPDATE_UI)) {
                    updateAdapter();
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver),
                new IntentFilter(Constants.UPDATE_UI)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    private List<String> getAllMessages() {
        List<String> messages = new ArrayList<String>();

        Cursor cursor = getContentResolver().query(Uri.parse(MyContentProvider.messagesTableUri), new String[]{},
                DatabaseHelper.SENDER + "=? OR " + DatabaseHelper.RECEIVER + "=?", new String[]{targetUser, targetUser}, "");

        while (cursor.moveToNext()) {
            String sender = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.SENDER));
            String message = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.MESSAGE));
            messages.add(sender + ": " + message);
        }
        cursor.close();

        return messages;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        UserListActivity.unselectUser();
    }
}
