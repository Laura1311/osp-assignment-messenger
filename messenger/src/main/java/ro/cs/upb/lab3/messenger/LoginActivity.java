package ro.cs.upb.lab3.messenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;


public class LoginActivity extends Activity {

    public static Context context;

    public static CommunicationService communicationService;
    private ServiceConnection serviceConnection;
    private EditText username;
    private EditText password;
    boolean mBound = false;
    public static Boolean logged = false;

    public static CommunicationService getCommunicationService() {
        return communicationService;
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            ResponseObject response = bundle.getParcelable(Constants.SERVER_RESPONSE);

            Log.d("LoginActivity", "mHandler");
            //check for errors
            if (response.getResponseCode() != 0) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                dialog.setTitle("User/Password incorrect");
                dialog.setMessage(response.getResponseMessage());

                AlertDialog alertDialog = dialog.create();
                alertDialog.show();

                // Restart the service. This is needed to close
                // the socket from the service and open it with a new try
                unbindService(serviceConnection);
                Intent i = new Intent(context, CommunicationService.class);
                bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
            } else {
                //redirect to user list
                Intent intent = new Intent(context, UserListActivity.class);

                intent.putExtra(Constants.USER_LIST, (ArrayList<String>) response.getUserList());
                intent.putExtra(Constants.LOGGED_USER, username.getText().toString());
                startActivity(intent);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        Button registerButton = (Button) findViewById(R.id.register_button);
        Button loginButton = (Button) findViewById(R.id.login_button);


        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                CommunicationService.MyBinder myBinder = (CommunicationService.MyBinder) service;
                communicationService = myBinder.getService();
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        Intent i = new Intent(this, CommunicationService.class);
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RegisterActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().isEmpty()
                        || password.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Username and password cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    RegisterObject registerObject = new RegisterObject(username.getText().toString(),
                            password.getText().toString(), false);

                    if (communicationService != null) {
                        communicationService.loginUser(registerObject, mHandler);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mBound) {
            unbindService(serviceConnection);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        // This usually happens after a logout.
        // We need this so we can login again
        unbindService(serviceConnection);
        Intent i = new Intent(context, CommunicationService.class);
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
    }
}
