package ro.cs.upb.lab3.messenger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @version $Id$
 */
public class Constants {
    public static final String RECEIVE_ACTION = "RECEIVE_ACTION";
    public static final String INPUT = "INPUT";

    public static final String USERNAME = "user";
    public static final String PASSWORD = "pass";
    public static final String CREATE = "create";
    public static final String TARGET_USERS = "users";
    public static final String SENT_MESSAGE = "message";
    public static final String INCOMING_MESSAGE = "incomingMessage";
    public static final String LOGGED_USER = "loggedUser";
    public static final String LOGGED_PASS = "password";
    public static final String USER_LIST = "userList";
    public static final String SERVER_RESPONSE = "serverResponse";
    public static final int INVALID_SERVER_RESPONSE = -1;
    public static final String UPDATE_UI = "ro.cs.upb.lab3.messenger.CommunicationService.UPDATE_UI";
    //public static final String SERVER_IP = "10.0.2.2";
    public static final String SERVER_IP = "192.168.1.102";
    public static final int SERVER_PORT = 4000;



    public static final String REGISTER_OBJECT = "REGISTER_OBJECT";

    public static BufferedReader getReader(Socket socket) throws IOException {
        return new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public static PrintWriter getWriter(Socket socket) throws IOException {
        return new PrintWriter(socket.getOutputStream(), true);
    }

}
