package ro.cs.upb.lab3.messenger;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * @version $Id$
 */
public class MessageObject extends JSONObject implements Parcelable {

    private List<String> userList;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageObject(List<String> userList, String message) {
        this.userList = userList;
        this.message = message;
    }

    public MessageObject(Parcel in) {
        in.readStringList(userList);
        this.message = in.readString();
    }

    public JSONObject getOutgoingMessageObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(userList);
        jsonObject.put(Constants.TARGET_USERS, jsonArray);
        jsonObject.put(Constants.SENT_MESSAGE, this.message);

        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.userList);
        dest.writeString(this.message);
    }
    public static final Creator CREATOR = new Creator() {
        public MessageObject createFromParcel(Parcel in) {
            return new MessageObject(in);
        }

        public MessageObject[] newArray(int size) {
            return new MessageObject[size];
        }
    };

}
