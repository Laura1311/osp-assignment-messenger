package ro.cs.upb.lab3.messenger;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class MyContentProvider extends ContentProvider {

    public static final int TABLE1 = 1;
    public static final int TABLE2 = 2;
    public static final int ROW = 3;

    // Change the uriRoot to the proper package NAME
    public static final String uriRoot = "ro.cs.upb.osp.lab3.messenger";
    public static final String favusersTableUri = "content://" + uriRoot + "/favusers";
    public static final String messagesTableUri = "content://" + uriRoot + "/messages";

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static{
        uriMatcher.addURI(uriRoot, "favusers", TABLE1);
        uriMatcher.addURI(uriRoot, "favusers/#", TABLE1);
        uriMatcher.addURI(uriRoot, "messages", TABLE2);
        uriMatcher.addURI(uriRoot, "messages/#", TABLE2);
    }

    private DatabaseHelper dbHelper;

    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int ret = db.delete("favusers", null, null);

        if (ret >= 0) {
            // Appends the URI to the favusersTableUri path
            Uri noteUri = ContentUris.withAppendedId(Uri.parse(favusersTableUri), ret);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return ret;
        } else {
            throw new UnsupportedOperationException("Failed to delete " + uri);
        }
    }

    @Override
    public String getType(Uri uri) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowId = -1;
        switch (uriMatcher.match(uri)) {
            case TABLE1:
                rowId = db.insert("favusers", null, values);
                if (rowId >= 0) {
                    // Appends the URI to the favusersTableUri path
                    Uri noteUri = ContentUris.withAppendedId(Uri.parse(favusersTableUri), rowId);
                    getContext().getContentResolver().notifyChange(noteUri, null);
                    return noteUri;
                }
                break;
            case TABLE2:
                rowId = db.insert("messages", null, values);
                if (rowId >= 0) {
                    // Appends the URI to the favusersTableUri path
                    Uri noteUri = ContentUris.withAppendedId(Uri.parse(messagesTableUri), rowId);
                    getContext().getContentResolver().notifyChange(noteUri, null);
                    return noteUri;
                }
                break;
        }

        return null;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();


        switch (uriMatcher.match(uri)) {
            case TABLE1:
                queryBuilder.setTables("favusers");
                break;
            case TABLE2:
                queryBuilder.setTables("messages");
                break;
            case ROW:
                queryBuilder.appendWhereEscapeString("id = " + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("WRONG URI: " + uri);
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        return queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
